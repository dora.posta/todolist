import React from 'react';
import './App.css';
import Button from './components/button';
import Navbar from './components/navbar';
import StickyNote from './components/sticky-note';
import { IList } from './types/list';
import { useState } from 'react';

const todoList: Record<string, IList> = {
  'monday': {
    items: ['Mosás', 'Főzés', 'Tanulás'],
  },
  'tuesday': {
    items: ['Főzés', 'Tanulás'],
  },
  'wednesday': {
    items: ['Főzés', 'Tanulás'],
  }
};



function App() {
  const [notes, setNotes] = useState<IList[]>([]);

  function addNote() {
    setNotes([
      ...notes, {
        items: [],
      }]);
  }


  return (
    <div className="App">
      <Navbar links={[
        { name: 'Home', href: '/' },
        { name: 'Friends', href: '/friends' },
        { name: 'Messages', href: '/messages' }
      ]} />
      <main>
        <Button color="secondary" className="newList" onClick={addNote}>Új lista hozzáadása</Button>
        <div className="notes">
          {
            notes.map((list,index) =>
              <StickyNote key={index} todolist={list}/>
            )
          }
        </div>
      </main>
    </div>
  );
}

export default App;
