import React from "react";

interface IState {
    counter: number;
}

class CounterButton extends React.Component<{}, IState> {
    constructor(props: {}) {
        super(props);
        this.state = { counter: 0 };

        this.increment = this.increment.bind(this);
    }

    render() {
        return (
            <>
                <button onClick={this.increment}>Click</button>
                {!!this.state.counter && <div>{this.state.counter}</div>}
            </>
        )
    }

    increment() {
        this.setState({
            counter: this.state.counter + 1,
        });
    }
}

export default CounterButton;