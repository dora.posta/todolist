import { useState } from "react";

function CounterButton() {
    const [counter, setCounter] = useState(0);
    const increment = () => setCounter(counter + 1);

    return (
        <>
            <button onClick={increment}>Click Me</button>
            {
                !!counter && <div className="red">{counter}</div>
            }
        </>
    );
}

export default CounterButton;