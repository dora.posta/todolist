import './button.css';

interface IButtonProps extends React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    color: 'primary' | 'secondary';
}


function Button(props: IButtonProps) {
    const {
        color,
        children,
        className = '',
        ...rest
    } = props;

    return (
        <button className={color + " " + className} {...rest}>
            {children}
        </button>
    )
}


export default Button;