import './navbar.css';

interface ILink {
    name: string;
    href: string;
}

interface IProps {
    links: ILink[];
}

function Navbar(props: IProps) {
    const { links } = props;

    return (
        <nav>
            <ul className="navbar">
                {
                    links.map(link =>
                        <li key={link.name}>
                            <a href={link.href}>{link.name}</a>
                        </li>
                    )
                }
            </ul>
        </nav>
    );
}

export default Navbar;