import './sticky-note.css';
import React from 'react';
import List from './list/list';
import { IList } from '../../types/list';
import Button from '../button';
import { useState } from 'react';

interface IProps{
    todolist: IList
}

function StickyNote(props: IProps) {
    const [creating, setCreating] = useState(false);
    const onItemCreated = () => {
        setCreating(false);
    }
    function addItem(): void {
        setCreating(true);
    }

    return (
        <div className="note">
            <List add={creating} listItems={props.todolist.items} onItemCreated={onItemCreated} />
            <Button color="primary" className="newItem" onClick={addItem}>
                Új elem hozzáadása
            </Button>
        </div>
    );
}





export default StickyNote;