import { useState } from 'react';
import './list-item.css'

interface IProps{
    name: string;
}

function ListItem({name}: IProps) {
    const [checked, setChecked] = useState(false);
    function changeState() {
        setChecked(!checked);
    }
    const textClass = checked ? 'checked': '';

    return (
        <li>
            <input type="checkbox" onChange={changeState} />
            <p className={textClass}>{name}</p>
        </li>
    );
}


export default ListItem;