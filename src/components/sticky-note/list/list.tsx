import { useState } from "react";
import ListItem from "./list-item";

interface IListItems {
    listItems: string[];
    add: boolean;
    onItemCreated: () => void;
}

function List(props: IListItems) {
    const {
        listItems,
        add,
        onItemCreated,
    } = props;
    const [items, setItems] = useState(listItems);

    function addItem(event: React.FocusEvent<HTMLInputElement>) {
        setItems([...items, event.currentTarget.value]);
        onItemCreated();
    }

    return (
        <ul>
            {
                items.map(item =>
                    <ListItem key={item} name={item} />
                )
            }
            {
                add &&
                <li>
                    <input type="checkbox" disabled />
                    <input type="text" onBlur={addItem} />
                </li>
            }
        </ul>
    );
}



export default List;